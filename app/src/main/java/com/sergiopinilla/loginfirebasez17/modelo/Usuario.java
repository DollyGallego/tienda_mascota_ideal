package com.sergiopinilla.loginfirebasez17.modelo;

public class Usuario {
    private String nombre,telefono,direccion,fechaCumpleaños,correo;

    public Usuario() {
    }

    public Usuario(String nombre, String telefono, String direccion, String fechaCumpleaños, String correo) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.fechaCumpleaños = fechaCumpleaños;
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaCumpleaños() {
        return fechaCumpleaños;
    }

    public void setFechaCumpleaños(String fechaCumpleaños) {
        this.fechaCumpleaños = fechaCumpleaños;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
