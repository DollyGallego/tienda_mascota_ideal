package com.sergiopinilla.loginfirebasez17.fragmentos;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.sergiopinilla.loginfirebasez17.R;

import java.util.Calendar;


public class AgregarFragment extends Fragment {

    private ImageView imgMascota;
    private Spinner spinnerTipoMascota;
    private EditText edNombreMascota,edFechaNacimientoMascota,edDireccionMascota,edDescripcionMascota;
    private CheckBox cbVacunasMascota;
    private RadioButton rbMachoMascota,rbHembraMascota;
    private Button btnAgregarMascotra;

    private final int REQUEST_CODE_PERMISSIONS = 0;
    private final int REQUEST_CODE_CAMARA = 1;
    private final int REQUEST_CODE_GALERIA = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agregar, container, false);
        imgMascota = view.findViewById(R.id.imgMascota);
        spinnerTipoMascota = view.findViewById(R.id.spinnerTipoMascotaMascotas);
        edNombreMascota = view.findViewById(R.id.edNombreMascota);
        edFechaNacimientoMascota = view.findViewById(R.id.edFechaNacimientoMascota);
        edDireccionMascota = view.findViewById(R.id.edDireccionMascota);
        edDescripcionMascota = view.findViewById(R.id.edDescripcionMascota);
        cbVacunasMascota = view.findViewById(R.id.cbVacunasMascota);
        rbMachoMascota = view.findViewById(R.id.rbMachoMascota);
        rbHembraMascota = view.findViewById(R.id.rbHembraMascota);
        btnAgregarMascotra = view.findViewById(R.id.btnAgregarMascotra);

        //Configuracion de spinner

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(),R.array.tipoMascotas, android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoMascota.setAdapter(adapter);

        //Configuracion evento onCLick
        imgMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturarImagen();
            }
        });
        btnAgregarMascotra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviarFormulario();
            }
        });
        edFechaNacimientoMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturarFechaCumpleaños();
            }
        });
        return view;

    }

    private void capturarFechaCumpleaños() {

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                    edFechaNacimientoMascota.setText(year+"-"+(month+1)+"-"+dayOfMonth);
                }
            },year,month,dayOfMonth);
            dialog.show();

    }

    private void enviarFormulario() {
        String genero;
        String tipoDeMascota = spinnerTipoMascota.getSelectedItem().toString();
        String nombreMascota = edNombreMascota.getText().toString();
        String fechaNacimientoMascota = edFechaNacimientoMascota.getText().toString();
        String direccionMascota = edDireccionMascota.getText().toString();
        String descripcionMascota = edDescripcionMascota.getText().toString();
        boolean vacunas = cbVacunasMascota.isChecked();
        if(rbMachoMascota.isChecked()){
            genero = "Macho";
        }else{
            genero = "Hembra";
        }

        //Todo Verificar imagen
        if(verificarCampos(nombreMascota)){
            //Logica para enviar a la base de datos
        }



    }

    private boolean verificarCampos(String nombreMascota) {
        if(nombreMascota.isEmpty()){
            edNombreMascota.setError("campo requerido");
            return false;
        }else{
            return true;
        }
    }

    private void capturarImagen() {
        boolean permisoCamara = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PermissionChecker.PERMISSION_GRANTED;
        boolean permisoGaleria = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PermissionChecker.PERMISSION_GRANTED;
        if(permisoCamara && permisoGaleria){
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext())
                    .setTitle("Captura de imagen")
                    .setMessage("Seleccione un medio para obtener la imagen")
                    .setNegativeButton("camara", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                            startActivityForResult(intent,REQUEST_CODE_CAMARA);
                        }
                    })
                    .setPositiveButton("galeria", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent,REQUEST_CODE_GALERIA);
                        }
                    });
            alert.show();
        }else{
            ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE_PERMISSIONS);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && data!=null){
            switch (requestCode){
                case REQUEST_CODE_CAMARA:
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    imgMascota.setImageBitmap(bitmap);
                    break;
                case REQUEST_CODE_GALERIA:
                    Uri uriImagenGaleria = data.getData();
                    imgMascota.setImageURI(uriImagenGaleria);
                    break;
            }
        }
    }
}