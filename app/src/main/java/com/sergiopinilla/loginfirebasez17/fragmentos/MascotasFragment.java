package com.sergiopinilla.loginfirebasez17.fragmentos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.sergiopinilla.loginfirebasez17.R;
import com.sergiopinilla.loginfirebasez17.adapters.MascotaAdapter;
import com.sergiopinilla.loginfirebasez17.modelo.Mascota;

import java.util.LinkedList;
import java.util.List;

public class MascotasFragment extends Fragment {

    private RecyclerView recyclerViewMascotas;
    private Spinner spinnerTipoMascotaMascotas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mascotas, container, false);
        recyclerViewMascotas = view.findViewById(R.id.recyclerViewMascotas);
        spinnerTipoMascotaMascotas = view.findViewById(R.id.spinnerTipoMascotaMascotas);

        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(getContext(),R.array.tipoMascotas, android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoMascotaMascotas.setAdapter(arrayAdapter);

        MascotaAdapter mascotaAdapter = new MascotaAdapter(obtenerMascotas());
        recyclerViewMascotas.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewMascotas.setAdapter(mascotaAdapter);

        return view;
    }

    //String nombre, String fechaNacimiento, String genero, int idImage
    //todo a futuro sera por medio de la db
    private List<Mascota> obtenerMascotas(){
        LinkedList<Mascota> listaMascotas = new LinkedList<>();
        listaMascotas.add(new Mascota("Perro1","2022-01-15","Macho",R.drawable.perro));
        listaMascotas.add(new Mascota("Gato1","2021-08-31","Macho",R.drawable.gato));
        listaMascotas.add(new Mascota("Conejo1","2020-10-25","Macho",R.drawable.conejo));
        return listaMascotas;
    }
}