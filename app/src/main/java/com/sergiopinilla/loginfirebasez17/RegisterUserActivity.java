package com.sergiopinilla.loginfirebasez17;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sergiopinilla.loginfirebasez17.modelo.Usuario;

public class RegisterUserActivity extends AppCompatActivity {

    private EditText edNombreRegister,edTelefonoRegister,edDireccionRegister,edFechaNacimientoRegister,edCorreoRegister,edContraseñaRegister;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        edNombreRegister = findViewById(R.id.edNombreRegister);
        edTelefonoRegister = findViewById(R.id.edTelefonoRegister);
        edDireccionRegister = findViewById(R.id.edDireccionRegister);
        edFechaNacimientoRegister = findViewById(R.id.edFechaNacimientoRegister);

        edContraseñaRegister = findViewById(R.id.edContraseñaRegister);
        edCorreoRegister = findViewById(R.id.edCorreoRegister);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public void registrarUsuario(View view){
        String nombre = edNombreRegister.getText().toString();
        String telefono = edTelefonoRegister.getText().toString();
        String direccion = edDireccionRegister.getText().toString();
        String fechaCumpleaños = edFechaNacimientoRegister.getText().toString();
        String email = edCorreoRegister.getText().toString();
        String password = edContraseñaRegister.getText().toString();



        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            String id = currentUser.getUid();
                            Usuario usuario = new Usuario(nombre,telefono,direccion,fechaCumpleaños,email);
                            mDatabase.child("usuario").child(id).setValue(usuario);
                            Intent intent = new Intent(RegisterUserActivity.this,HomeActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(RegisterUserActivity.this,"No se creo el usuario",Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}