package com.sergiopinilla.loginfirebasez17.controlador;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.sergiopinilla.loginfirebasez17.fragmentos.AgregarFragment;
import com.sergiopinilla.loginfirebasez17.fragmentos.FavoritosFragment;
import com.sergiopinilla.loginfirebasez17.fragmentos.MascotasFragment;

public class PagerController extends FragmentPagerAdapter {

    private int numDeItemTab;

    public PagerController(@NonNull FragmentManager fm, int numDeItemTab) {
        super(fm,numDeItemTab);
        this.numDeItemTab = numDeItemTab;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new MascotasFragment();
            case 1: return new AgregarFragment();
            case 2: return new FavoritosFragment();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return numDeItemTab;
    }
}
