package com.sergiopinilla.loginfirebasez17;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText edCorreoLogin,edContraseñaLogin;
    private FirebaseAuth mAuth;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edContraseñaLogin = findViewById(R.id.edContraseñaLogin);
        edCorreoLogin = findViewById(R.id.edCorreoLogin);
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("es");
        mDialog = new ProgressDialog(this);


    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            lanzarHome();
        }
    }

    public void iniciarSesion(View view){
        String email = edCorreoLogin.getText().toString();
        String password = edContraseñaLogin.getText().toString();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("Inicio sesion sactisfactorio", "signInWithEmail:success");
                            lanzarHome();
                        } else {
                            Log.w("Inicio sesion fallido", "signInWithEmail:success");
                            Toast.makeText(LoginActivity.this,"Login fallido verificar credenciales",Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void registro(View view){
        Intent intent = new Intent(LoginActivity.this,RegisterUserActivity.class);
        startActivity(intent);
    }

    private void lanzarHome(){
        Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void olvidoContraseña(View view){

        String email = edCorreoLogin.getText().toString();
        if(email.isEmpty()){
            edCorreoLogin.setError("El correo es requerido");
        }else{
            mDialog.setMessage("Estamos trabajando para usted...");
            mDialog.setCanceledOnTouchOutside(true);
            mDialog.show();
            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        mDialog.dismiss();
                        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Restaurar contraseña")
                                .setMessage("Se envio el correo con el link de restauracion")
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                        alert.show();
                    }else{
                        mDialog.dismiss();
                        Log.e("ErrorSendEmail",task.getException().toString());
                        Toast.makeText(LoginActivity.this,"No se logro enviar el correo",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }
}