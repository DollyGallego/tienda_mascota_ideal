package com.sergiopinilla.loginfirebasez17.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sergiopinilla.loginfirebasez17.R;
import com.sergiopinilla.loginfirebasez17.modelo.Mascota;

import java.util.List;

public class MascotaAdapter extends RecyclerView.Adapter<MascotaAdapter.ViewHolder> {

    List<Mascota> mascotaList;

    public MascotaAdapter(List<Mascota> mascotaList) {
        this.mascotaList = mascotaList;
    }

    @NonNull
    @Override
    public MascotaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_mascotas,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MascotaAdapter.ViewHolder holder, int position) {
        holder.txtNombreMascotaCard.setText(mascotaList.get(position).getNombre());
        holder.txtGeneroMascotaCard.setText(mascotaList.get(position).getGenero());
        holder.txtNacimientoMascotaCard.setText(mascotaList.get(position).getFechaNacimiento());
        holder.imgMascotaCard.setImageResource(mascotaList.get(position).getIdImage());
        //Todo falta logica para el tema de favoritos
    }

    @Override
    public int getItemCount() {
        return mascotaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNombreMascotaCard,txtGeneroMascotaCard,txtNacimientoMascotaCard;
        private ImageView imgMascotaCard,imgFavoritosCard;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgFavoritosCard = itemView.findViewById(R.id.imgFavoritosCard);
            imgMascotaCard = itemView.findViewById(R.id.imgMascotaCard);
            txtNombreMascotaCard = itemView.findViewById(R.id.txtNombreMascotaCard);
            txtGeneroMascotaCard = itemView.findViewById(R.id.txtGeneroMascotaCard);
            txtNacimientoMascotaCard = itemView.findViewById(R.id.txtNacimientoMascotaCard);

        }
    }
}
